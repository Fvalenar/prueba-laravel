@extends('layouts.app')

@section('content')

 <h1>Payment Detail</h1>

<h4 class="text-center"><strong>Total: </strong>${{$order->total}} </h4>

        <div class="text-center mb-3">
            <form
                method="POST"
                class="d-inline"
                action="{{route('orders.payments.store', ['order'=> $order->id]) }} ">
                @csrf

                <button class="btn btn-success" type="submit">Pay</button>

            </form>
        </div>


@endsection


