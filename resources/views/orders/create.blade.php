@extends('layouts.app')

@section('content')

 <h1>Order Detail</h1>

<h4 class="text-center"><strong>Total: </strong>${{$cart->total}} </h4>

        <div class="text-center mb-3">
            <form
                method="POST"
                class="d-inline"
                action="{{route('orders.store') }} ">
                @csrf

                <button class="btn btn-success" type="submit">Confirm your Order</button>

            </form>
        </div>
        <div class="table-responsive ">
            <table class="table table-striped ">
                <thead class="thead-light">
                    <tr>

                        <th>Title</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>

                    </tr>
                </thead>
                <tbody >
                    @foreach ($cart->products as $product)
                    <tr>

                        <td>
                            <img src="{{ asset($product->images->first()->path) }} " width="100" class="mr-5">
                            {{ $product->title}}
                        </td>
                        <td >{{ $product->price}} </td>
                        <td>{{ $product->pivot->quantity}} </td>
                        <td>
                            <strong>
                                ${{$product->total }}
                            </strong>
                        </td>

                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

@endsection


