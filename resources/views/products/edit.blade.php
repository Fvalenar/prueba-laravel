@extends('layouts.app')

@section('content')
    <h1>Edit a Product</h1>

    <form action="{{route('products.update', ['product' => $product -> id])}} " method="POST">

        @csrf
        @method('PUT')
        <div class="form-row">
            <label >Title</label>
            <input type="text" name="title"  class="form-control" value="{{old('title') ?? $product -> title}} ">
        </div>
        <div class="form-row">
            <label >Description</label>
            <input type="text" name="description" required class="form-control"
            value="{{old('description') ?? $product->description}} ">
        </div>

        <div class="form-row">
            <label >Price</label>
            <input class="form-control" type="number"  min="1.00" step="0.01" name="price"
            value="{{old('price') ?? $product->price}}" required >
        </div>
        <div class="form-row">
            <label >Stock</label>
            <input class="form-control" type="number"  min="0" name="stock"
            value="{{old('stock') ?? $product->stock}}" required >
        </div>

        <div class="form-row">
            <label >Status</label>
            <select class="custom-select" name="status" required>
                <option {{ old('status') == 'available' ? 'selected': ($product->status ==
                 'available' ? 'selected' : '') }} value="available">Available</option>
                <option {{ old('status') == 'unavailable' ? 'selected': ($product->status ==
                'unavailable' ? 'selected': '') }} value="unavailable">Unavailable</option>
            </select>
        </div>

        <div class="form-row mt-3">
            <button type="submit" class="btn btn-primary btn-lg ">Edit Product</button>
        </div>


    </form>

@endsection