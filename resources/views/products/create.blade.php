@extends('layouts.app')

@section('content')
    <h1>Create a Product</h1>

    <form action="{{route('products.store')}} " method="POST">
        @csrf
        <div class="form-row">
            <label >Title</label>
            <input type="text" name="title"  class="form-control"  value="{{ old('title')}}">
        </div>
        <div class="form-row">
            <label >Description</label>
            <input type="text" name="description" required class="form-control" value="{{ old('description')}}">
        </div>
        <div class="form-row">
            <label >Price</label>
            <input required class="form-control" type="number"  min="1.00" step="0.01" name="price"
            value="{{ old('price') }}" >
        </div>
        <div class="form-row">
            <label >Stock</label>
            <input required class="form-control" type="number"  min="0" name="stock"  value="{{ old('stock')}}" >
        </div>

        <div class="form-row">
            <label >Status</label>
            <select class="custom-select" name="status" required value="{{ old('')}} ">
                <option value="" selected>Select...</option>
                <option {{ old('status') == 'available' ? 'selected': ""}} value="available">Available</option>
                <option {{ old('status') == 'unavailable' ? 'selected': "" }} value="unavailable">Unavailable</option>
            </select>
        </div>

        <div class="form-row mt-3">
            <button type="submit" class="btn btn-primary btn-lg">Create Product</button>
        </div>


    </form>

@endsection