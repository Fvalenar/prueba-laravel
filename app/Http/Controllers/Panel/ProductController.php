<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Product;

class ProductController extends Controller {

	public function index() {
		$products = Product::all();
		//$products = DB::table('products')->get();
		//dd($products);

		return view('products.index')->with([
			'products' => Product::all(),
		]);
	}

	public function create() {

		return view('products.create');

	}

	public function store(ProductRequest $request, Product $product) {

		$product = Product::create($request->validated());

		//return redirect()->back();
		//return redirect()->action('MainController@index');
		return redirect()->route('products.index')
			->withSuccess("The Product {$product->title} was created successfully");

	}

	public function show(Product $product) {
		//$product = DB::table('products')->find($product);
		//$product = Product::findOrFail($product); //

		return view('products.show')->with([
			'product' => $product,
		]);
	}

	public function edit(Product $product) {
		//$pr = Product::findOrFail($product);
		//dd($pr);
		return view('products.edit')->with([
			'product' => $product,
		]);

	}

	public function update(ProductRequest $request, Product $product) {

		//$product = Product::findOrFail($product);
		$product->update($request->validated());

		return redirect()
			->route('products.index')
			->withSuccess("The Product {$product->title} was edited successfully");
	}

	public function destroy(Product $product) {

		//$product = Product::findorFail($product);
		$product->delete();
		return redirect()
			->route('products.index')
			->withSuccess("The Product {$product->id} was deleted");

	}

}
